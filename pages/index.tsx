import type { NextPage } from 'next'
import { useEffect, useState } from 'react'
import Image from 'next/image'
import styles from '../styles/Home.module.css'


const Home: NextPage = () => {
  const [word, setWord] = useState<string>('Ready for the next software generation?');

  useEffect(() => {
    var w: string = word;
    window.setInterval(() => {
      w = w?.slice(0, -1);
      setWord(w || 'Complex & Custom UI; Blockchain and AI solutions');
    }, 200)
  }, [])

  return (
    <div className={styles.container}>
      <div>
        <Image src="/awe1.svg" height="72" width="72" alt="awL" />
      </div>
      <div>{word} <span className={styles.blinkEraser}>_</span></div>
    </div>
  )
}

export default Home
